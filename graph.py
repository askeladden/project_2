import sys
import numpy as np
import matplotlib.pyplot as plt

filename = sys.argv[1]

plt.plotfile(filename, color='red', cols=(3, 0), delimiter=',')
plt.plotfile(filename, color='blue', cols=(3, 1), newfig=False, delimiter=',')
plt.plotfile(filename, color='green', cols=(3, 2), newfig=False, delimiter=','),


plt.xlabel(r'$\rho$');
plt.ylabel(r'$\Psi^2$');

plt.savefig(filename+".pdf")