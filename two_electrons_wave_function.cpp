#include <iostream>
#include <armadillo>
#include <time.h>
#include <fstream>
#include <iomanip> //setw
#include <math.h>  /* sqrt */

using namespace std;
using namespace arma;


////////###   FUNCTIONS BEGIN ###/////////
 
// write to file function; 2 arguments - filename, matrix

void write_to_file ( char *filename, mat MATRIX, int N )
{
     ofstream ofile;
     ofile.open(filename, ios::app);
     for (int i = 0; i < N; i++) {
             ofile << setprecision(8) << MATRIX(i,0)<<","<<MATRIX(i,1)<<","<<MATRIX(i,2)<<","<<MATRIX(i,3)<<endl;
     }
     ofile.close();
     return;
}

////////###  END OF FUNCTIONS  ###/////////


int main ( int argc, char *argv[] )
{
   if ( argc != 5 ) {
       cout<<"Usage: "<<argv[0]<<" matrixsize ro_max omega filename" <<endl;
       abort();
    }

    double omega, omega_sq, near_diag_element, diag_element_minus_v, h, h_squared, ro_max, ro_min;
    char *file;
    int N;
    vec eigval;
    mat eigvec;
    ro_min = 1.0e-8;
    N = atof(argv[1]);
    ro_max = atof(argv[2]);
    omega = atof(argv[3]);
    file = argv[4];


    mat   A(N,N);
    rowvec RO(N);
    rowvec  V(N);
    rowvec  ro(N);
    mat    PSI(N,N);
    // calculate discrete ro values
    omega_sq=omega*omega;
    h = (ro_max - ro_min)/N;
    h_squared = h*h;
    for (int i = 0; i < N; i++) {
        RO(i) = ro_min + i*h;
        V(i)  = RO(i)*RO(i)*omega_sq + 1.0/RO(i);
    }

    // Fill A matrix
    near_diag_element = -1.0/h_squared;
    diag_element_minus_v = 2.0/h_squared;
    A.fill(0.0);
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            if (i == j) A(i,j) = diag_element_minus_v + V(i);
            if (abs(i - j) == 1) A(i,j) = near_diag_element;
        }
    }

   eig_sym( eigval, eigvec, A );
        for (int i = 0; i < N; i++) {
             PSI(i,0) = eigvec(i,0)*eigvec(i,0);
             PSI(i,1) = eigvec(i,1)*eigvec(i,1);
             PSI(i,2) = eigvec(i,2)*eigvec(i,2);
             PSI(i,3)=ro_min+i*h; //It ro column
         }
   write_to_file( file, PSI, N );

  return 0;
  }
