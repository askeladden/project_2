#include <iostream>
#include <armadillo>
#include <time.h>
#include <fstream>
#include <iomanip> //setw
#include <math.h>  /* sqrt */

using namespace std;
using namespace arma;


////////###   FUNCTIONS BEGIN ###/////////
 
// MAX element of matrix
/*returns absolute max element of a matrix 
and modifies global vars - indexes of max element (k,l)
*/
double max_offdiag ( mat MATRIX, int  &k, int &l, int N ) 
{
       double max = 0.0;
       for ( int i = 0; i < N; i++ ) {
           for ( int j = i + 1; j < N; j++ ) {
               if ( fabs(MATRIX(i,j)) > max ) {
                  max = fabs(MATRIX(i,j));
                  k = i;
                  l = j;
               }
           }
       }
       return max;
}
// end of MAX element of matrix

// write to file function; 2 arguments - filename, matrix
void write_to_file ( char *filename, rowvec MATRIX, int n, int N, double J_time, double A_time, int iters )
{
     ofstream ofile;
     ofile.open(filename, ios::app);
     ofile <<"Number of iterations "<<iters<<endl;
     ofile <<"Matrix size "<<N<<endl;
     ofile << setprecision(8) <<"Armadillo time   "<<A_time<<endl;
     ofile << setprecision(8) <<"Jacobi rotations "<<J_time<<endl;
     ofile << setprecision(8) <<"Armadillo time "<<A_time<<endl;
     ofile << "Eigenvalues" <<endl;
     for (int i = 0; i < n; i++) {
         ofile << setprecision(8) << MATRIX(i)<<endl;
     }
     ofile <<"================================="<<endl;
     ofile.close();
     return;
}


//Jacobi rotation method implementation
/* Takes matrix by reference, indexes of matrix max element and matrix size
Calculates B matrix elements after similarity transformations
then changes original matrix with rotated */
void Jacobi_algorythm ( mat &A, int k, int l, int N )
{
     double s, c, t, tau;
     if ( A(k,l) != 0.0 ) {
        tau = (A(l,l) - A(k,k))/(2*A(k,l));
        if ( tau >= 0 ) {
            t = 1.0/(tau + sqrt(1.0 + tau*tau));
        } else { t = -1.0/(-tau +sqrt(1.0 + tau*tau)); }
        c = 1/sqrt(1+t*t);
        s = c*t;
        } else { c = 1.0; s = 0.0;
     }
     double a_kk, a_ll, a_ik, a_il;
     a_kk = A(k,k);
     a_ll = A(l,l);
     A(k,k) = c*c*a_kk - 2.0*c*s*A(k,l) + s*s*a_ll;
     A(l,l) = s*s*a_kk + 2.0*c*s*A(k,l) + c*c*a_ll;
     A(k,l) = 0.0;  // hard-coding non-diagonal elements by hand
     A(l,k) = 0.0;  // same here
     for ( int i = 0; i < N; i++ ) {
         if ( i != k && i != l ) {
            a_ik = A(i,k);
            a_il = A(i,l);
            A(i,k) = c*a_ik - s*a_il;
            A(k,i) = A(i,k);
            A(i,l) = c*a_il + s*a_ik;
            A(l,i) = A(i,l);
    }

  }

  return;
}
// end of Jacobi rotate


void test_unit ()
{
    mat test_matrix;
    int k, l, N;
    test_matrix << 10 << 19 << 8 << 7 << -20 << endr
                << 10 << 9 << 8 << 7 << 5 << endr
                << 10 << 9 << 8 << 7 << 5 << endr
                << 10 << 9 << 8 << 7 << 5 << endr
                << 10 << 9 << 10 << 7 << 5 << endr;
     N=5; k = 0; l = 0;
     max_offdiag ( test_matrix, k, l, N);
     if (k == 0 && l == 4) cout<<"max_offdiag test OK"<<endl;
     test_matrix.print(); 

return;
}



////////###  END OF FUNCTIONS  ###/////////


int main (int argc, char *argv[])
{
    //test_unit();
    
    //input must be matrix_size ro_max filename
    if ( argc != 4 ) {
       cout<<"Usage: "<<argv[0]<<" matrixsize ro_max filename" <<endl;
       abort();
    }
    double Jacobi_time, armadillo_time, epsilon, near_diag_element, diag_element_minus_v, h, h_squared, ro_max, ro_min, iterations;
    int k, l, N;
    char *file;
    clock_t timer_start, timer_finish; //declare start/stop time
    vec eigval;
    mat eigvec;
    iterations = 0;
    epsilon = 1.0e-8;
    ro_min = 0.0;
    N = atof(argv[1]);
    ro_max = atof(argv[2]);
    file = argv[3];

    mat   A(N,N);
    rowvec RO(N);
    rowvec  V(N);
    rowvec  lambda(N);
    rowvec  lambda_sort(N);
    // calculate discrete ro values
    h = (ro_max - ro_min)/N;
    h_squared = h*h;
    for (int i = 0; i < N; i++) {
        RO(i) = ro_min + i*h;
        V(i)  = RO(i)*RO(i);
    }

    // Fill A matrix
    near_diag_element = -1.0/h_squared;
    diag_element_minus_v = 2.0/h_squared;
    A.fill(0.0);
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            if (i == j) A(i,j) = diag_element_minus_v + V(i);
            if (abs(i - j) == 1) A(i,j) = near_diag_element;
        }
    }
    timer_start = clock(); // Armadillo check
    eig_sym( eigval, A );
    timer_finish = clock();// End of armadillo
    armadillo_time=(timer_finish - timer_start)/(double)CLOCKS_PER_SEC;

    timer_start = clock(); // Jacobi start
    while ( fabs(max_offdiag(A, k, l, N)) > epsilon ) {
          max_offdiag (A, k, l, N);
          Jacobi_algorythm ( A, k, l, N );
          iterations++;
    }
    timer_finish = clock(); // Jacobi stop
    Jacobi_time=(timer_finish - timer_start)/(double)CLOCKS_PER_SEC;

    for (int i = 0; i < N; i++) {
        lambda(i) = A(i,i);
    }
    lambda_sort = sort(lambda);
    write_to_file( file, lambda_sort, 3, N, Jacobi_time, armadillo_time, iterations );

    return 0;
}
