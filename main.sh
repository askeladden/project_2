#!/bin/bash

#set -x

export PATH=$PATH:$(pwd)

make && make clean

#case 1
#matrixsize ro_max filename variables on input
ro_max=10
filename="one_electron_eigenvalues"
matrix_size=(50 100 200 300 400)

for i in ${matrix_size[@]}
    do
      echo "matrix size is $i"
      if [ $i -eq 300 ] || [ $i -eq 400 ]; then
         echo "Please dont interrup. It can take several minutes to complete Jacobi rotations ....."
      fi
      one_electron_jacobi_eigenval $i $ro_max $filename
    done

#case 2
#matrixsize ro_max omega filename variables on input

filename="wave_function_two_electrons_omega"
matrix_size=200
omega=(0.01 0.5 1 5)
ro_max_arr=(50 7 5 2)

for (( i=0; i < ${#omega[@]}; i++))
    do
      two_electrons_wave_function $matrix_size ${ro_max_arr[i]} ${omega[i]} $filename"_${omega[i]}"
      python graph.py $filename"_${omega[i]}"
      rm -f $filename"_${omega[i]}"
    done


