CC=g++ -Wall
#PROG= two_electrons_wave_function
LIB = -llapack -lblas -larmadillo

all: two_electrons_wave_function one_electron_jacobi_eigenval
two_electrons_wave_function : two_electrons_wave_function.o
	${CC} two_electrons_wave_function.o ${LIB} -o two_electrons_wave_function

one_electron_jacobi_eigenval : one_electron_jacobi_eigenval.o
	${CC} one_electron_jacobi_eigenval.o ${LIB} -o one_electron_jacobi_eigenval

two_electrons_wave_function.o : two_electrons_wave_function.cpp
	${CC} -c two_electrons_wave_function.cpp

one_electron_jacobi_eigenval.o : one_electron_jacobi_eigenval.cpp
	${CC} -c one_electron_jacobi_eigenval.cpp
clean:
	rm -f *.o